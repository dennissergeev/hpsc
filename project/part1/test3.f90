
program test

    use mpi

    use quadrature, only: trapezoid
    use functions, only: f, fevals_proc, k

    implicit none
    real(kind=8) :: a, b, int_true, int_approx, int_sub
    real(kind=8) :: dx, ab_sub(2)

    integer :: proc_num, num_procs, ierr, n, fevals_total
    integer :: nsub, nsent, sender, nextsub
    integer :: j
    integer, dimension(MPI_STATUS_SIZE) :: status

    logical :: debug

    debug = .true.

    call MPI_INIT(ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, num_procs, ierr)
    call MPI_COMM_RANK(MPI_COMM_WORLD, proc_num, ierr)

    ! All processes set these values so we don't have to broadcast:
    k = 1.d3   ! functions module variable 
    n = 1000   ! points for trapezoid integration (per subinterval)

    if (num_procs == 1) then
        print *, "*** Error: need to use at least two processes"
        go to 999
        endif

    ! Each process keeps track of number of fevals:
    fevals_proc = 0

    if (proc_num == 0) then
    ! Process 0 is the master process
        a = 0.d0
        b = 2.d0
        int_true = (b-a) + (b**4 - a**4) / 4.d0 - (1.d0/k) * (cos(k*b) - cos(k*a))
        int_approx = 0.d0 ! initialize variable for accumulating result later

        print '("Using ",i3," processes")', num_procs
        print *, "How many subintervals? "
        read *, nsub
        print *, " "  ! blank line
        print '("true integral: ", es22.14)', int_true
        print *, " "  ! blank line

    endif

    call MPI_BARRIER(MPI_COMM_WORLD,ierr) ! wait for process 0 to print
 
    call MPI_BCAST(nsub, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
    
    if (proc_num == 0) then

        nsent = 0 ! counter for sent subintervals

        dx = (b-a)/nsub ! splitting the interval between slave processes
        do j = 1, min(nsub, num_procs-1)
            ! Computing boundaries for subintervals and sending them to slave processes
            ab_sub(1) = a + (j-1)*dx
            ab_sub(2) = a + j*dx
            call MPI_SEND(ab_sub,2,MPI_DOUBLE_PRECISION, j, j, &
                          MPI_COMM_WORLD, ierr)
            nsent = nsent + 1
        enddo

      ! as results come back, send out more work...
      ! the variable sender tells who sent back a result and ready for more
        do j = 1, nsub
            ! Receive the computed integral from a slave process
            call MPI_RECV(int_sub, 1, MPI_DOUBLE_PRECISION, &
                          MPI_ANY_SOURCE, MPI_ANY_TAG, &
                          MPI_COMM_WORLD, status, ierr)
            int_approx = int_approx + int_sub

            sender = status(MPI_SOURCE)
            
            if (nsent < nsub) then
                ! send more
                nextsub = nsent + 1
                ab_sub(1) = a + (nextsub-1)*dx
                ab_sub(2) = a + nextsub*dx
                call MPI_SEND(ab_sub,2,MPI_DOUBLE_PRECISION, sender, nextsub, &
                              MPI_COMM_WORLD, ierr)
                nsent = nsent + 1
            else
                ! send an empty message with tag=0 to indicate that process is done
                call MPI_SEND(MPI_BOTTOM, 0, MPI_INTEGER, &
                              sender, 0, MPI_COMM_WORLD, ierr)
            endif
        enddo
        print *, 'Master process finished...'
        print *, ' '
        print *, 'nsent =', nsent
    endif


    ! 
    ! Code for slaves
    ! 
    if (proc_num /= 0) then

        if (proc_num > nsub) then
            go to 2015 ! no work for this process
        endif

        do while (.true.)
            ! Receive until message with tag 0 received

            call MPI_RECV(ab_sub, 2, MPI_DOUBLE_PRECISION, &
                      0, MPI_ANY_TAG, & ! ANY_TAG because it can hang
                      MPI_COMM_WORLD, status, ierr)
            j = status(MPI_TAG) ! subinterval number
    
            if (debug) then
                print '("+++ Process ",i4,"  received message with tag ",i6)', &
                    proc_num, j       
                endif
            if (j==0) then 
                go to 2015 ! received 'done' message
            endif
    
            ! Compute the integral
            int_sub = trapezoid(f,ab_sub(1),ab_sub(2),n)
            ! Send the result to the master process
            call MPI_SEND(int_sub, 1, MPI_DOUBLE_PRECISION, &
                          0, j, MPI_COMM_WORLD, ierr)
        enddo
    endif

2015 continue

    call MPI_BARRIER(MPI_COMM_WORLD,ierr) ! wait for all process to finish

    ! print the number of function evaluations by each thread:
    print '("fevals by Process ",i2,": ",i13)',  proc_num, fevals_proc

    call MPI_BARRIER(MPI_COMM_WORLD,ierr) ! wait for all process to print


    call MPI_REDUCE(fevals_proc, fevals_total, 1, MPI_INTEGER, MPI_SUM, &
                    0, MPI_COMM_WORLD, ierr)

    ! Note: In this version all processes call trap and repeat the
    !       same work, so each should get the same answer.  
    if (proc_num==0) then
        print '("Trapezoid approximation with ",i8," total points: ",es22.14)', &
               n*nsub, int_approx
!        print '("Total number of fevals: ",i10)', fevals_total
    endif

999 continue
    call MPI_FINALIZE(ierr)

end program test
