module quadrature_mc

contains

function quad_mc(g,a,b,ndim,npoints)

    implicit none

    integer, intent(in) :: ndim, npoints
    real(kind=8), external :: g
    real(kind=8), intent(in) :: a(ndim), b(ndim)

    real(kind=8) :: quad_mc ! the function result

    integer :: i

    real(kind=4), allocatable :: r(:), rnext(:)
    real(kind=8) :: gsum, vol
    real(kind=8), allocatable :: x(:)

    allocate(r    (ndim*npoints))
    allocate(x    (ndim        ))
    allocate(rnext(ndim        ))

    gsum = 0.
    vol = product(b-a)

    call random_number(r)
    do i = 1, npoints
        rnext = r((i-1)*ndim + 1 : i*ndim)
        x = a + rnext*(b-a) ! ndim elements of x
        gsum = gsum + g(x,ndim) ! calling external function g
    enddo

    quad_mc = vol*gsum/npoints

    deallocate(r    )
    deallocate(x    )
    deallocate(rnext)
end function quad_mc

end module quadrature_mc 
