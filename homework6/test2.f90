
program test

    use mpi

    use quadrature, only: trapezoid
    use functions, only: f, fevals_proc, k

    implicit none
    real(kind=8) :: a, b, int_true, int_approx, int_sub
    real(kind=8) :: dx, ab_sub(2)

    integer :: proc_num, num_procs, ierr, n, fevals_total
    integer :: j
    integer, dimension(MPI_STATUS_SIZE) :: status

    call MPI_INIT(ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, num_procs, ierr)
    call MPI_COMM_RANK(MPI_COMM_WORLD, proc_num, ierr)

    ! All processes set these values so we don't have to broadcast:
    k = 1.d3   ! functions module variable 
    n = 1000   ! points for trapezoid integration (per subinterval)
    
    ! Each process keeps track of number of fevals:
    fevals_proc = 0

    if (proc_num == 0) then
    ! Process 0 is the master process
        a = 0.d0
        b = 2.d0
        int_true = (b-a) + (b**4 - a**4) / 4.d0 - (1.d0/k) * (cos(k*b) - cos(k*a))
        int_approx = 0.d0 ! initialize variable for accumulating result later

        print '("Using ",i3," processes")', num_procs
        print '("true integral: ", es22.14)', int_true
        print *, " "  ! blank line

    endif

    call MPI_BARRIER(MPI_COMM_WORLD,ierr) ! wait for process 0 to print
 
    
    if (proc_num == 0) then
        dx = (b-a)/(num_procs-1) ! splitting the interval between slave processes
        do j = 1, num_procs-1
            ! Computing boundaries for subintervals and sending them to slave processes
            ab_sub(1) = a + (j-1)*dx
            ab_sub(2) = a + j*dx
            call MPI_SEND(ab_sub,2,MPI_DOUBLE_PRECISION, j, j, &
                          MPI_COMM_WORLD, ierr)
        enddo

        do j = 1, num_procs-1
            ! Receive the computed integral from all processes
            call MPI_RECV(int_sub, 1, MPI_DOUBLE_PRECISION, &
                          MPI_ANY_SOURCE, MPI_ANY_TAG, &
                          MPI_COMM_WORLD, status, ierr)
            int_approx = int_approx + int_sub
        enddo
    endif

    ! 
    ! Code for slaves
    ! 
    if (proc_num /= 0) then
        call MPI_RECV(ab_sub, 2, MPI_DOUBLE_PRECISION, &
                      0, MPI_ANY_TAG, & ! ANY_TAG because it can hang
                      MPI_COMM_WORLD, status, ierr)
        j = status(MPI_TAG) ! subinterval number

        ! Compute the integral
        int_sub = trapezoid(f,ab_sub(1),ab_sub(2),n)
        ! Send the result to the master process
        call MPI_SEND(int_sub, 1, MPI_DOUBLE_PRECISION, &
                      0, j, MPI_COMM_WORLD, ierr)
    endif


    call MPI_BARRIER(MPI_COMM_WORLD,ierr) ! wait for all process to finish

    ! print the number of function evaluations by each thread:
    print '("fevals by Process ",i2,": ",i13)',  proc_num, fevals_proc

    call MPI_BARRIER(MPI_COMM_WORLD,ierr) ! wait for all process to print


    call MPI_REDUCE(fevals_proc, fevals_total, 1, MPI_INTEGER, MPI_SUM, &
                    0, MPI_COMM_WORLD, ierr)

    ! Note: In this version all processes call trap and repeat the
    !       same work, so each should get the same answer.  
    if (proc_num==0) then
        print '("Trapezoid approximation with ",i8," total points: ",es22.14)', &
               n*(num_procs-1), int_approx
        print '("Total number of fevals: ",i10)', fevals_total
    endif

    call MPI_FINALIZE(ierr)

end program test
