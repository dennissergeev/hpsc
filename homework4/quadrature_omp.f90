module quadrature

use omp_lib

contains

real(kind=8) function trapezoid(f,a,b,n)
    ! Estimate the integral of f(x) from a to b using the
    ! Trapezoid Rule with n points.
    !
    ! Inputs/Outputs
    !  
    ! Variable   I/O   Description
    ! --------   ---   -----------
    ! f          I     Function to integrate
    ! a          I     Left endpoint
    ! b          I     Right endpoint
    ! n          I     Number of points
    ! trapezoid  O     The estimate of the integral

    implicit none
    real(kind=8), intent(in ) :: a, b
    real(kind=8), external    :: f
    integer     , intent(in ) :: n
    ! Local variables
    real(kind=8) :: h, int_sum, xi
    integer :: i
    
    h = (b-a)/(n-1)
    int_sum = 0.5d0*(f(a)+f(b))

    !$omp parallel do private(xi) reduction(+ : int_sum)
    do i = 2, n-1
        xi = a + (i-1)*h
        int_sum = int_sum + f(xi)
    enddo
    trapezoid = h*int_sum
end function trapezoid

subroutine error_table(f,a,b,nvals,int_true,method)
    ! Loop over number of points 
    ! and get the integral estimation using the trapezoid rule.
    ! Print a table with results for each number of points 
    ! and error between the estimation and the true value of integral

    implicit none
    real(kind=8),          intent(in) :: a, b, int_true
    real(kind=8),          external   :: f, method
    integer, dimension(:), intent(in) :: nvals
    ! Local variables
    integer      :: i, n
    real(kind=8) :: ratio, last_error, error, int_trap

    print *, "    n         trapezoid            error       ratio"
    last_error = 0.  ! need something for first ratio
    do i = 1, size(nvals)
        n = nvals(i)
        int_trap = method(f,a,b,n)
        error = abs(int_trap - int_true)
        ratio = last_error / error
        last_error = error ! for next n
        print 11, n, int_trap, error, ratio
11      format(i8, es22.14, es13.3, es13.3)
    enddo
end subroutine error_table

end module quadrature
