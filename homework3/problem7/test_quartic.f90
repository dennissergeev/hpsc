! -*- coding: utf-8 -*-
!"""
!@author: D. E. Sergeev
!"""
program test_quartic

    use newton, only: solve, tol
    use my_functions, only: f_quartic, fprime_quartic, eps

    implicit none
    real(kind=8) :: x, x0, fx
    real(kind=8) :: x0val
    real(kind=8) :: xstar
    real(kind=8) :: tolerances(3)
    real(kind=8) :: epsilons(3)
    integer :: iters
    integer :: itol, ieps
    logical :: debug

    print *, "Quartic polynomial solver"
    debug = .false.

    xstar = 1.d0
    epsilons = (/1.d-4, 1.d-8, 1.d-12/)
    tolerances = (/1.d-5, 1.d-10, 1.d-14/)

    ! values to test as x0:
    x0val = 4.d0

    print *, ' '
    print *, '    epsilon        tol    iters          x                 f(x)        x-xstar'
    do ieps = 1, 3
        eps = epsilons(ieps)
        do itol = 1, 3
            tol = tolerances(itol)

            x0 = x0val
            call solve(f_quartic, fprime_quartic, x0, x, iters, debug)
            fx = f_quartic(x)

            print 11, eps, tol, iters, x, fx, x-xstar
 11         format(2es13.3, i4, es24.15, 2es13.3)
        enddo
        print *, ' '
    enddo

end program test_quartic
