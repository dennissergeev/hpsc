# -*- coding: utf-8 -*-
"""
@author: D. E. Sergeev
"""
maxiter = 20
tol = 1e-14

def solve(fvals, x0, debug):

    x = x0 # initial guess
    if debug:
        print "Initial guess: x = {:22.15e}".format(x)

    for i in xrange(maxiter-1):
        fx, fxp = fvals(x)
        
        # check if the function is close to zero
        if abs(fx) < tol:
            break
        
        # compute the increment
        dx = fx/fxp

        # update x
        x -= dx
        
        if debug:
            print "After {0} iteration(s) x = {1:22.15e}".format(i+1,x)

    if i >= maxiter-1:
        # if not converged
        fx, = fvals(x)
        
        if abs(fx) > tol:
            print "*** Warning: did not converge after {0} iterations".format(i+1)
    
    return x, i+1

def fvals_sqrt(x):
    """
    Return f(x) and f'(x) for applying Newton to find a square root.
    """
    f = x**2 - 4.
    fp = 2.*x
    return f, fp

def test1(debug_solve=False):
    """
    Test Newton iteration for the square root with different initial
    conditions.
    """
    from numpy import sqrt
    for x0 in [1., 2., 100.]:
        print " "  # blank line
        x, iters = solve(fvals_sqrt, x0, debug=debug_solve)
        print "solve returns x = %22.15e after %i iterations " % (x, iters)
        fx, fpx = fvals_sqrt(x)
        print "the value of f(x) is %22.15e" % fx
        assert abs(x-2.) < 1e-14, "*** Unexpected result: x = %22.15e"  % x
