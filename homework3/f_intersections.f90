! -*- coding: utf-8 -*-
!"""
!@author: D. E. Sergeev
!"""
program f_intersections

    use newton, only: solve
    use my_functions, only: df, df_prime, g1, g2

    implicit none
    real(kind=8) :: x, x0, fx
    real(kind=8) :: x0vals(4)
    integer :: iters, itest
    logical :: debug         ! set to .true. or .false.

    print *, "Program to find the intersection points of the two functions"
    debug = .false.

    ! values to test as x0:
    x0vals = (/-2.1, -1.6, -0.8, 1.5/)

    do itest=1,4
        x0 = x0vals(itest)
        print *, ' '  ! blank line
        call solve(df, df_prime, x0, x, iters, debug)

        print 11, x, iters
11      format('solver returns x = ', e22.15, ' after', i3, ' iterations')

        fx = g1(x)
        print 12, fx
12      format('the value of f(x) is ', e22.15)

        enddo

end program f_intersections
