# -*- coding: utf-8 -*-
"""
@author: D. E. Sergeev
"""
import matplotlib.pyplot as plt
import numpy as np
import newton as N

def gval1(x):
    g = x*np.cos(np.pi*x)
    gp = np.cos(np.pi*x) - x*np.pi*np.sin(np.pi*x)
    return g, gp

def gval2(x):
    g = 1. - 0.6*x**2
    gp = -1.2*x
    return g, gp

def fval_diff(x, f1, f2):
    """
    Return f(x)=f_1(x)-f_2(x) and f'(x)=f_1'(x)-f_2'(x)
    """
    g1, gp1 = f1(x)
    g2, gp2 = f2(x)
    f = g1 - g2
    fp = gp1 - gp2
    return f, fp

def gval_diff(x):
    """
    Return f(x)=f_1(x)-f_2(x) and f'(x)=f_1'(x)-f_2'(x)
    """
    g1, gp1 = gval1(x)
    g2, gp2 = gval2(x)
    f = g1 - g2
    fp = gp1 - gp2
    return f, fp

def find_intersect(x0, f1=gval1, f2=gval2, func=gval_diff, debug=False):
    """
    Find the intersections between two functions and plot the results
    """
    xx = []
    ff = []
    if hasattr(x0, '__iter__'):
        for j, ix in enumerate(x0):
            print ""
            x, iters = N.solve(func, ix, debug)
            print "x{0} = {1:22.15e} after {2} iterations".format(j+1, x, iters)
            xx.append(x)
            ff.append(f1(x)[0])
    else:
        print ""
        x, iters = N.solve(func, x0, debug)
        print "x = {0:22.15e} after {1} iterations".format(x, iters)
        xx.append(x)
        ff.append(f1(x)[0])

    # Plot the functions over some interval 
    # with the intersections marked with black dots.
    #
    # Then saves the figure to a .png file
    fig, ax = plt.subplots(figsize=(15,12))
    ax.grid()
    x_inter = np.arange(-5.,5.01,0.01)
    ax.plot(x_inter, f1(x_inter)[0], 'b', label='$f_1(x)$')
    ax.plot(x_inter, f2(x_inter)[0], 'r', label='$f_2(x)$')
    ax.plot(xx, ff, 'ko')

    ax.legend()
 
    fig.savefig('intersections.png')
