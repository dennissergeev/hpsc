! -*- coding: utf-8 -*-
!"""
!@author: D. E. Sergeev
!"""
module my_functions
    implicit none
    real(kind=8), parameter :: pi = acos(-1.)

contains

real(kind=8) function df(x)
    implicit none
    real(kind=8), intent(in) :: x
    df = g1(x) - g2(x)
end function df

real(kind=8) function df_prime(x)
    implicit none
    real(kind=8), intent(in) :: x
    df_prime = g1_prime(x) - g2_prime(x)
end function df_prime

real(kind=8) function g1(x)
    implicit none
    real(kind=8), intent(in) :: x
    g1 = x*cos(pi*x)
end function g1

real(kind=8) function g1_prime(x)
    implicit none
    real(kind=8), intent(in) :: x
    g1_prime = cos(pi*x) - pi*x*sin(pi*x)
end function g1_prime

real(kind=8) function g2(x)
    implicit none
    real(kind=8), intent(in) :: x
    g2 = 1. - 0.6*x**2
end function g2

real(kind=8) function g2_prime(x)
    implicit none
    real(kind=8), intent(in) :: x
    g2_prime = - 1.2*x
end function g2_prime

! Old code
real(kind=8) function f_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x

    f_sqrt = x**2 - 4.d0

end function f_sqrt


real(kind=8) function fprime_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x
    
    fprime_sqrt = 2.d0 * x

end function fprime_sqrt

end module my_functions
